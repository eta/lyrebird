module gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/lyrebird

require (
	filippo.io/edwards25519 v1.0.0
	github.com/dchest/siphash v1.2.3
	github.com/refraction-networking/utls v1.5.3
	gitlab.com/yawning/edwards25519-extra.git v0.0.0-20220726154925-def713fd18e4
	gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/goptlib v1.5.0
	golang.org/x/crypto v0.13.0
	golang.org/x/net v0.15.0
)

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/cloudflare/circl v1.3.3 // indirect
	github.com/gaukas/godicttls v0.0.4 // indirect
	github.com/klauspost/compress v1.16.7 // indirect
	github.com/quic-go/quic-go v0.37.4 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.13.0 // indirect
)

go 1.19
